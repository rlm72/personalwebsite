"""
A module to create cards for Anki using images from web searches.

Images are currently sourced from [pixabay](https://pixabay.com), and are therefore in the public domain.
"""
import os
import pickle
import logging
import concurrent.futures
from io import BytesIO
from collections import defaultdict

import requests
from PIL import Image

logging.basicConfig(level=logging.INFO)


def defaultdict_int():
    """
    Helper function that allows the defaultdict used in Requester to be pickled.
    """
    return defaultdict(int)


def defaultdict_list():
    """
    Helper function that allows the defaultdict used in SimpleCache to be pickled.
    """
    return defaultdict(list)


class CacheInformation:
    """
    Store information about an images cache.
    """
    def __init__(self, images_per_search):
        self.images_per_search = images_per_search

    def __repr__(self):
        return 'CacheInformation({})'.format(repr(self.images_per_search))


class SimpleCache:
    """
    A simple cache for requests to the pixabay API.

    Should be safe to concurrently run multiple `Requesters` with.
    """
    main_directory = 'cache'    # the directory used to store cache data
    contents_filename = 'cache_contents.pkl'
    cache_information_filename = 'cache_information.pkl'
    image_id_filename = 'image_id.dat'

    def __init__(self, images_per_search, max_workers=3):
        try:
            self.contents = self.read_contents_file()
            self.information = self.read_information_file()

            if not self.validate() or (images_per_search != self.information.images_per_search):
                self.information.images_per_search = images_per_search
                self.reset()

        except FileNotFoundError:        # being initialised for the first time
            # contents maps searches to page: (filename list) dictionaries.
            self.contents = defaultdict(defaultdict_list)
            self.information = CacheInformation(images_per_search)
            os.mkdir(type(self).main_directory)
            with open(type(self).image_id_filename, 'w') as f:
                f.write(str(0))

            self.write_contents_file()
            self.write_information_file()

        self.max_workers = max_workers

    def new_image_id(self):
        """
        Return an unused image ID.
        """
        with open(type(self).image_id_filename) as f:
            value = int(f.read())

        with open(type(self).image_id_filename, 'w') as f:
            f.write(str(value+1))

        return str(value)

    def read_contents_file(self):
        """
        Load the file detailing the cache contents into a set.
        """
        with open(os.path.join(type(self).main_directory, type(self).contents_filename), 'rb') as f:
            result = pickle.load(f)

        return result

    def write_contents_file(self):
        """
        Write the contents dictionary into the contents file.
        """
        with open(os.path.join(type(self).main_directory, type(self).contents_filename), 'wb') as f:
            pickle.dump(self.contents, f)

    def read_information_file(self):
        """
        Load the file containing cache information into a CacheInformation obejct.
        """
        with open(os.path.join(type(self).main_directory, type(self).cache_information_filename), 'rb') as f:
            result = pickle.load(f)

        return result

    def write_information_file(self):
        """
        Write the cache information into the cache information file.
        """
        with open(os.path.join(type(self).main_directory, type(self).cache_information_filename), 'wb') as f:
            pickle.dump(self.information, f)

    def validate(self):
        """
        Check that the stored files match the cache information.
        """
        try:
            new_contents = self.read_contents_file()
        except FileNotFoundError:
            logging.warning("Cache validation failed: contents file couldn't be found")
            return False

        # TODO: maybe actually do some validation for the new structure
        return True

    def process(self, search):
        """
        Strip characters from a search to make it a valid directory name.
        """
        result = search.lower()
        result = ''.join([c for c in result if c.isalnum()])
        return result

    def get_by_filename(self, filename):
        """
        Return the Image in the given file.
        """
        return Image.open(os.path.join(self.main_directory, '{}.png'.format(filename)))

    def get(self, search, page):
        """
        Return a list of Images for a given search if found in the cache, or `None` if not found.
        """
        self.contents = self.read_contents_file()
        filenames = self.contents[self.process(search)][page]
        if filenames:
            return [(filename, self.get_by_filename(filename)) for filename in filenames]
        else:
            logging.info('"{}" page {} not in cache'.format(search, page))
            return None

    def image_saving_function(self, search, page):
        """
        Return a function that saves images to files specified by IDs.
        """
        def save_image(image_id, image):
            """
            Save an image to a file given by an id.
            """
            self.contents[self.process(search)][page].append(image_id)
            image.save(os.path.join(type(self).main_directory, '{}.png'.format(image_id)))

        return save_image

    def add(self, search, images, page):
        """
        Add a list of Images for a given search to the cache (updating it if they are already present).
        """
        self.contents = self.read_contents_file()
        image_ids = [self.new_image_id() for image in images]

        with concurrent.futures.ThreadPoolExecutor(max_workers=self.max_workers) as executor:
            executor.map(self.image_saving_function(search, page), image_ids, images)

        self.write_contents_file()

    def reset(self):
        for thing in os.scandir(type(self).main_directory):
            if thing.is_file() and thing.name.endswith('png'):
                os.remove(thing.path)

        self.contents = defaultdict(defaultdict_list)
        self.write_contents_file()
        self.write_information_file()


class Requester:
    """ Make requests to the pixabay API, caching repeated requests.

    `cache_class` represents the cache class that `Requester` objects will use.
    Instances of that class should have a `get` method that takes a query and returns a list of images (if in the cache)
    and `None` otherwise, and an `add` method that takes a query and a list of images that maps to it and puts the pair
    into the cache.

    Also receives requests from clients specifying the image they found best matched a query.
    This will not necessarily be one that was found by searching for that query (for instance, the best result for
    'cheese' might actually have come from searching 'swiss cheese').
    """

    cache_class = SimpleCache
    url = 'https://pixabay.com/api/'

    recommended_images_filename = 'recommended_images.pkl'    # stores data of images that API users have chosen as good

    def __init__(self, api_key, images_per_search, max_workers=6, cache_max_workers=6):
        self.api_key = api_key
        self.cache = type(self).cache_class(images_per_search, max_workers=cache_max_workers)
        self.recommended_images = defaultdict(defaultdict_int)
        self.write_recommended_images()
        self.max_workers = max_workers

    @property
    def images_per_search(self):
        """
        Shorthand to get `images_per_search` from the cache.
        """
        return self.cache.information.images_per_search

    @images_per_search.setter
    def images_per_search(self, value):
        self.cache.information.images_per_search = value
        self.cache.write_information_file()

    def read_recommended_images(self):
        """
        Load the recommended images dictionary from a file.
        """
        with open(type(self).recommended_images_filename, 'rb') as f:
            return pickle.load(f)

    def write_recommended_images(self):
        """
        Write the recommended images dictionary to a file.
        """
        with open(type(self).recommended_images_filename, 'wb') as f:
            pickle.dump(self.recommended_images, f)

    def url_to_image_function(self, s):
        """
        Return a function that loads images from URLS using session object `s`.
        """
        def url_to_image_map(url):
            """
            Load an image from a URL with a session object `s` and add it to a list.
            """
            image = Image.open(BytesIO(s.get(url).content))
            image.thumbnail((320, 320))
            return image

        return url_to_image_map

    def request(self, search, page=1):
        """
        Return a list of (filename, Image) tuples that match a query, from the cache if possible.
        """
        result = self.cache.get(search, page)
        if result is not None:
            return result

        else:
            params = {
                'key': self.api_key,
                'q': search,
                'safesearch': 'true',
                'page': page,
                'per_page': self.cache.information.images_per_search,
                }

            with requests.Session() as s:
                r = s.get(type(self).url, params=params)
                content = r.json()
                hits = [hit['webformatURL'] for hit in content['hits']]

                with concurrent.futures.ThreadPoolExecutor(max_workers=self.max_workers) as executor:
                    result = executor.map(self.url_to_image_function(s), hits[:self.cache.information.images_per_search])

                self.cache.add(search, list(result), page)

                # return result from cache as that will have all images converted to PNGs
                return self.cache.get(search, page)

    def request_with_recommendations(self, search, recommended_number, page=1):
        """
        Does the same thing as `Recommender.request`, but will return up to `recommended_number` of images
        from `self.recommended_images` (sorted by recommendations) before making normal requests.
        """
        self.recommended_images = self.read_recommended_images()
        recommended = sorted(list(self.recommended_images[search].keys()),
                             key=lambda x: self.recommended_images[search][x],
                             reverse=True)[:recommended_number]

        used_filenames = set(recommended)
        result = [(name, self.cache.get_by_filename(name)) for name in recommended]

        # get images that aren't already in result until we have enough
        cache_page = 1
        while len(result) < (page * self.images_per_search):
            for (name, image) in self.request(search, page=cache_page):
                if name not in used_filenames:
                    used_filenames.add(name)
                    result.append((name, image))

            cache_page += 1

        return result[(page-1)*self.images_per_search:page*self.images_per_search]

    def recommend_image(self, original_search, image_id):
        """
        Process a recommendation of a specific image.
        """
        self.recommended_images = self.read_recommended_images()
        self.recommended_images[original_search][image_id] += 1
        self.write_recommended_images()
