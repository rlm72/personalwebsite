import logging
import zipfile
import concurrent.futures
from io import BytesIO
from pickle import load
from random import choice

from flask import Flask, Response, render_template, request, jsonify

import pixabay
import keys
from buzzphrases import buzzphrase

app = Flask(__name__)
app.url_map.strict_slashes = False

logging.basicConfig(level=logging.INFO)

USE_CONCURRENCY = True

pixabay_requester = pixabay.Requester(keys.pixabay, 6)


def render_base(url, **kwargs):
    return render_template(url, **kwargs)


@app.route('/')
def index():
    return render_base('index.html')


@app.route('/text_generation')
def text_generation():
    phrase = buzzphrase()
    with open('jokes.pkl', 'rb') as f:
        joke = choice(load(f))
    return render_base('text_generation.html', phrase=phrase, joke=joke)


@app.route('/poisson_prediction')
def poisson_prediction():
    return render_base('poisson_prediction.html')


def save_image(image_tuple):
    """
    Helper function for concurrent.futures.
    """
    image_file = BytesIO()
    image_tuple[1].save(image_file, format='PNG')
    return image_file.getvalue()


@app.route('/pixabay', methods=['GET'])
def pixabay_api():
    q = request.args.get('q')
    if q is None:
        return render_base('400.html'), 400
    else:
        page = request.args.get('page')
        if page is None:
            page = 1
        image_tuples = pixabay_requester.request_with_recommendations(q, 6, page=int(page))

        result = BytesIO()
        if USE_CONCURRENCY:
            with concurrent.futures.ThreadPoolExecutor(max_workers=6) as executor:
                image_file_values = executor.map(save_image, image_tuples)

            with zipfile.ZipFile(result, 'a', zipfile.ZIP_DEFLATED) as f:
                for index, image_file_value in enumerate(image_file_values):
                    f.writestr('{}.png'.format(image_tuples[index][0]), image_file_value)

        else:
            with zipfile.ZipFile(result, 'a', zipfile.ZIP_DEFLATED) as f:
                for (filename, image) in image_tuples:
                    image_file = BytesIO()
                    image.save(image_file, format='PNG')
                    f.writestr('{}.png'.format(filename), image_file.getvalue())

        result.seek(0)
        return Response(result, mimetype='application/zip')


@app.route('/pixabay', methods=['POST'])
def pixabay_app_image_recommend():
    q = request.values.get('q')
    filename = request.values.get('filename')
    if q is None or filename is None:
        logging.info('q {} filename {}'.format(q, filename))
        return render_base('400.html'), 400
    else:
        pixabay_requester.recommend_image(q, filename)
        return jsonify({'outcome': 'success'})


@app.errorhandler(404)
def page_not_found(e):
    return render_base('404.html'), 404
