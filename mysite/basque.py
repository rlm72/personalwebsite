"""
Some functions to help learn Basque.
"""
import random
from enum import Enum

class Person(Enum):
    ni = 0
    hi = 1
    hura = 2
    gu = 3
    zu = 4
    zuek = 5
    haiek = 6

THIRD = (Person.hura, Person.haiek)
PLURAL = (Person.gu, Person.zu, Person.zuek, Person.haiek)

NOR_PRONOUNS = ("ni", "hi", "hura", "gu", "zu", "zuek", "haiek")
NOR_PRESENT = ("naiz", "haiz", "da", "gara", "zara", "zarete", "dira")

NOR_NORI_NORS = ("n", "h", "", "g", "z", "z", "")

def nor_present(nor):
    return "{} {}".format(NOR_PRONOUNS[nor], NOR_PRESENT[nor])


def random_pronoun():
    return random.choice(NOR_PRONOUNS)

def random_pronoun_unique_pair():
    choices = list(NOR_PRONOUNS)
    random.shuffle(choices)
    return choices[0], choices[1]
