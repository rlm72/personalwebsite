"""
A simple script for predicting event occurrences with the Poisson distribution.
"""
from scipy.stats import poisson


def predict_range(current_count, time_elapsed, time_remaining, lower_bound, upper_bound):
    """
    Given how many events have occured so far in a period,
    predict the probability that the total number of events in the whole period will be within some bounds.
    Inclusive of endpoints.
    """
    periodic_rate = current_count / time_elapsed
    estimated_rate = periodic_rate * time_remaining
    minimum_count = lower_bound - current_count - 1
    maximum_count = upper_bound - current_count
    # if minimum_count < #events in time remaining <= maximum_count, prediction is true
    probability_too_low = poisson.cdf(minimum_count, estimated_rate)
    probability_not_too_high = poisson.cdf(maximum_count, estimated_rate)
    return probability_not_too_high - probability_too_low
