from random import choice

pre_adjectives = ["cryptographically secure",
                  "blockchain based",
                  "decentralized",
                  "provably correct",
                  "free",
                  "MOOC about",
                  "distributed",
                  "real-time",
                  "framework for",
                  "virtual reality",
                  "augmented reality",
                  "scalable",
                  "virtualized",
                  "concurrent",
                  ]

post_adjectives = ["as a service",
                   "in space",
                   "for farmers in developing countries",
                   "in Haskell",
                   "in Lisp",
                   "in Rust",
                   "for the N64",
                   "for startups",
                   "in the cloud",
                   "with genetic engineering",
                   "for small businesses",
                   "for programmers",
                   "to increase productivity",
                   "to circumvent censorship",
                   ]

technologies = ["CRISPR",
                "Uber",
                "virtual reality",
                "Tinder",
                "augmented reality",
                "Pokemon Go",
                "deep learning",
                "reinforcement learning",
                "AI",
                "Big Data",
                "biotechnology",
                "3D printing",
                "neural networks",
                "GPUs",
                "Wikileaks",
                "supercomputers",
                ]


def buzzphrase():
    """
    Generate a buzzphrase.
    """
    while True:
        attempt = " ".join([choice(pre_adjectives),
                            choice(technologies),
                            choice(post_adjectives),])

        if len(attempt.split()) == len(set(attempt.split())):
            return attempt


if __name__ == "__main__":
    print(buzzphrase())
